import React from 'react';
import { images } from '../../constants';
import { FiFacebook, FiTwitter, FiInstagram } from 'react-icons/fi';
import './FooterLinks.scss';

export default function FooterLinks() {
  return (
    <div className="app__footer-links">
      <div className="app__footer-links_contact">
        <h1 className="app__footer-headtext">Contact us</h1>
        <p className="p__opensans">111 street, 111 city, country</p>
        <p className="p__opensans">+123-456-789</p>
        <p className="p__opensans">+987-654-321</p>
      </div>
      <div className="app__footer-links_logo">
        <img src={images.gericht} alt="img-logo"/>
        <p className="p__opensans">The best way to find yourself is to lose yourself in the service of others.</p>
        <img src={images.spoon} className="spoon__img" style={{ marginTop: 15 }} alt="img-logo"/>
        <div className="app__footer-links_icons">
          <FiFacebook/>
          <FiInstagram />
          <FiTwitter />
        </div>
      </div>
      <div className="app__footer-links_work">
        <h1 className="app__footer-headtext">Working Hours</h1>
        <p className="p__opensans">Monday - Friday: </p>
        <p className="p__opensans">10:00 am - 06:00 pm</p>
        <p className="p__opensans">Saturday - Sunday</p>
        <p className="p__opensans">08:00 am - 10:00 pm</p>
      </div>
    </div>
  )
}
