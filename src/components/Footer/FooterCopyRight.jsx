import React from 'react'

export default function FooterCopyRight() {
  return (
    <div className="footer__copyright" style={{ marginTop: '3rem' }}>
      <p className="p__opensans">28/03/2022 Training CSS and responsive</p>
    </div>
  )
}
