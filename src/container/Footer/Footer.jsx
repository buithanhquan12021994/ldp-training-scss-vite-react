import React from 'react';
import { FooterOverlay, Newsletter, FooterLinks, FooterCopyRight } from '../../components';

import './Footer.scss';

const Footer = () => (
  <div className="app__footer section__padding">
    <FooterOverlay />
    <Newsletter/>
    <FooterLinks />
    <FooterCopyRight />
  </div>
);

export default Footer;
