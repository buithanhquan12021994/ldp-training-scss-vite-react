import React from 'react';

import { images } from '../../constants';
import { SubHeading } from '../../components';
import './Header.scss';

const Header = () => (
  <div className="app__header app__wrapper section__padding" id="home">
    <div className="app__wrapper_info">
      <SubHeading title="Chase the new flavour"/>
      <h1 className="app__header-1">The Key to Fine Dining</h1>
      <p className="p__opensans" style={{  margin: '2rem 0' }}>Text is here! type some text here to display them</p>
      <button type="button" className="custom__button">Explore menu</button>
    </div>
    <div className="app__wrapper_img">
      <img src={images.welcome} alt="welcome-img" />
    </div>
  </div>
);

export default Header;
