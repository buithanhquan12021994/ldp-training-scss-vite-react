import React from 'react';

import { SubHeading } from '../../components';
import { images } from '../../constants';

import './Chef.scss';

const Chef = () => (
  <div className="app__bg app__wrapper section__padding">
    <div className="app__wrapper_img app__wrapper_img-reverse">
      <img src={images.chef} alt="image-chef" />
    </div>

    <div className="app__wrapper_info">
      <SubHeading title="Chef's word" />
      <h1 className="headtext__cormorant">What we belive in</h1>

      <div className="app__chef-content">
        <div className="app__chef-content_quote">
          <img src={images.quote} alt="quote" />
          <p className="p__opensans">Sub content is here! Lorem is text for testing</p>
        </div>
        <p className="p__opensans">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
      </div>

      <div className="app__chef-sign">
        <p>Author chef</p>
        <p className="p__opensans">Chef & Founded</p>
        <img src={images.sign} alt="author"/>
      </div>
    </div>
  </div>
);

export default Chef;
